# AUMENTE A SEGURANÇA DE SUAS APLICAÇÕES COM **SOFTWARE LIVRE**
## Apresentação realizada na Campus Party Goiás em 2022

Este repositório armazena os slides utilizados na palestra intitulada "DevSecOps com OpenSource:  Análises automatizadas!" que foi apresentada no dia 25/03/2022 na Campus Party Goiás.

Os slides foram escritos utilizando Markdown e estilizados com **marp**. Quer saber mais?

Seguem links de referência:
* [https://github.com/marp-team/marp](https://github.com/marp-team/marp)
* [https://www.markdownguide.org/](https://www.markdownguide.org/)

Dentro do diretório "slides" existe um arquivo chamado "README.pdf" já estilizado. 

[Você pode clicar aqui para realizar o download do arquivo diretamente, caso prefira.](slides/README.pdf)

O material foi criado por mim, [Samuel Gonçalves](https://beacons.ai/sgoncalves). Caso queira entrar em contato, é [só clicar aqui](https://beacons.ai/sgoncalves).

Este material serve para que você o utilize, replique, referencie, e compartilhe conhecimento!!

### Viva o Software Livre!